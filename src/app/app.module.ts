import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginModule} from './login/login.module';
import {ClienteModule} from './cliente/cliente.module';
import {ServicesModule} from './shared/services/services.module';
import {ResolversModule} from './shared/resolvers/resolvers.module';
import {ModelsModule} from './shared/models/models.module';
import {ComponentsModule} from './shared/components/components.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    ClienteModule,
    ServicesModule,
    ResolversModule,
    ModelsModule,
    ComponentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
