import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CadastroComponent } from './cadastro/cadastro.component';
import { ClientesComponent } from './clientes/clientes.component';

@NgModule({
  declarations: [
    CadastroComponent,
    ClientesComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ClienteModule { }
